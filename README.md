# what is it doing ?
This project unlock the exam mode of the casio calculator by connecting them whit the usb port to the computer with this program.

# how to install it ?
- Create a directory named calculatrice_unlocker in /root.
- clone the repo in the directory previously created
- copy the .service file into /etc/systemd/system/
- enable the service with `systemctl enable calculatrice_unlocker`
- it is working

# Attention
This project was created under opensuse 15.1 and I don't know if it is working on some other distribution of linux.
