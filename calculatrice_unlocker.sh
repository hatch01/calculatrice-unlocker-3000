#!/bin/bash
while [ true ] ; do 

mnt="/root/calculatrice_unlocker/mnt"
calculatrice=$( fdisk -l | grep FAT12 | awk 'BEGIN { FX = " " } ; { print $1 }' )

if [ -n "$calculatrice" ] ; then
	echo $calculatrice	
	echo "calculatrice détecter"
	mount $calculatrice $mnt
	if [ -z $(ls /root/calculatrice_unlocker/mnt | grep exam_unlocker_3000) ]
	then
		echo "on créé le fichier exam_unlocker_3000"
		echo "exam_unlocker_3000" > $mnt/exam_unlocker_3000
	else	
		echo "on supprime le fichier exam_unlocker_3000"
		rm $mnt/exam_unlocker_3000
	fi
	echo "unmounting"
	umount $mnt
	echo "ejecting"
	eject=$(udisksctl power-off -b $calculatrice)
	echo $eject
	while [ -n "$eject" ]
	do	
		echo "ejecting"
		eject=$(udisksctl power-off -b $calculatrice)
	done
fi

done
